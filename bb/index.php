<?php

require 'autoload.php';

use Construction\Construction;

define('AREA', 'A');
define('ACCOUNT_TYPE', 'admin');

require __DIR__.'/../../init.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

if (!defined('DEVELOPMENT')) {
    define('DEVELOPMENT', true);
}

include 'source/index.html';
if (!empty($_REQUEST['addon_name'])) {
    fn_create_addon_construction($_REQUEST['addon_name']);
}

function fn_create_addon_construction($addon_name)
{
    $construction = new Construction($addon_name);
}
