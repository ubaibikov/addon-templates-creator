<?php

namespace Construction\Puts;

use Construction\AddonPuts;

class PutAddonFileTempates
{
    protected $templateFilePath = '';

    public function putAddonTemplateFiles(array $lbs)
    {
        foreach ($lbs  as $fileKey => $file) {
            $this->templateFilePath = $this->getTemplatePath($fileKey);
            if ($this->isFileTemplate($fileKey)) {
                $currentTemplatePath = $this->getCurrentTemplatePath($fileKey);
                $fileContext = $this->getFileContext($currentTemplatePath);
                $this->putFileContext($file, $fileContext);
            }
        }
    }

    private function getTemplatePath(string $fileKey): string
    {
        return AddonPuts::getAddonTemplatesPath();
    }

    protected function getFileContext(string $file): string
    {
        $handle = file_get_contents($file);

        return $handle;
    }

    protected function putFileContext(string $path, string $context): bool
    {
        return file_put_contents($path, $context, true) ? true : false;
    }

    protected function isFileTemplate(string $fileKey): bool
    {
        $currentFilePath = $this->templateFilePath.$fileKey;

        return is_dir($currentFilePath);
    }

    protected function getCurrentTemplatePath(string $fileKey)
    {
        $currentFilePath = $this->templateFilePath.$fileKey.AddonPuts::getAddonValue($fileKey);

        return $currentFilePath;
    }
}
