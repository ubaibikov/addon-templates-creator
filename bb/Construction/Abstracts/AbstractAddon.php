<?php

namespace Construction\Abstracts;

abstract class AbstractAddon
{
    /**
     * @param array $addon : is a addon paths
     */
    protected static $addon = [];
}
