<?php

namespace Construction\Copies;

use Construction\AddonCopies;
use Construction\CreateHlprs\CreateHlpr;

class CopyAddonFiles
{
    public function createCopyFiles(array $paths, string $copyKey)
    {
        foreach ($paths as $pathKey => $path) {
            if ($this->isCurrentAddonPath($copyKey, $pathKey)) {
                $this->copyMediaAddonsFile($this->getCoppiedAddonFilePath($pathKey), $path);
            }
        }
    }

    protected function isCurrentAddonPath(string $searchKey, string $mediaKey): bool
    {
        return CreateHlpr::currentPathKey($searchKey, $mediaKey);
    }

    protected function getCoppiedAddonFilePath(string $key)
    {
        return AddonCopies::getAddonCopiesValue($key);
    }

    protected function copyMediaAddonsFile(string $coppiedFile, string $coppyPath): bool
    {
        return copy($coppiedFile, $coppyPath);
    }
}
