<?php

namespace Construction;

use Construction\Copies\CopyAddonFiles;
use Construction\CreateHlprs\CreateHlpr;
use Construction\Creates\CreateAddonFlds;
use Construction\Creates\CreateAddonFls;
use Construction\Puts\PutAddonFileTempates;

class Construction
{
    private $addonName = '';

    public function __construct($addonName)
    {
        $this->addonName = $addonName;
        $this->createConstruction();
    }

    protected function createConstruction()
    {
        $createFolders = $this->createFolders(new CreateAddonFlds());

        if ($createFolders->getState()) {
            $createFiles = $this->createFiles(new CreateAddonFls());

            if ($createFiles['create_files']->getState()) {
                $this->copyFiles(new CopyAddonFiles(), $createFiles['fls_data']);
                $this->putTemplates(new PutAddonFileTempates(), $createFiles['fls_data']);
            }
        } else {
            fn_print_die($createFolders->getMessages());
        }
    }

    private function createFolders(CreateAddonFlds $createFolders)
    {
        $flds_data = $this->currentAddonNameLabels(AddonFlds::getAddonLabels());
        $createFolders->create($flds_data);

        return $createFolders;
    }

    private function createFiles(CreateAddonFls $createFiles)
    {
        $fls_data = $this->currentAddonNameLabels(AddonFls::getAddonLabels());
        $createFiles->create($fls_data);

        return ['create_files' => $createFiles, 'fls_data' => $fls_data];
    }

    private function putTemplates(PutAddonFileTempates $putTemplates, array $files)
    {
        $putTemplates->putAddonTemplateFiles($files);
    }

    protected function copyFiles(CopyAddonFiles $copyFiles, array $folderPaths)
    {
        $copyFiles->createCopyFiles($folderPaths, 'copy');
    }

    protected function currentAddonNameLabels(array $addonFlds): array
    {
        return CreateHlpr::createCurrentAddonLabels($addonFlds, '[addon_name]', $this->addonName);
    }
}
