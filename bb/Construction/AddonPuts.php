<?php

namespace Construction;

use Construction\Abstracts\AbstractAddon;
use Construction\Interfaces\AddonSys;

class AddonPuts extends AbstractAddon implements AddonSys
{
    protected static $addon = [
        'addon' => '/addon_xml.txt',
        'func'  => '/func.txt',
        'init'  => '/init.txt',
        'en'    => '/en.txt',
        'ru'    => '/ru.txt',
    ];

    protected static $addonTemplatesPath = 'templates/';

    public static function getAddonLabels(): array
    {
        return self::$addon;
    }

    public static function setAddonLabel(string $key, string $value)
    {
        $addon = self::$addon;

        if (!empty($addon[$key])) {
            $addon[$key] = $value;
        } else {
            return;
        }
    }

    public static function getAddonTemplatesPath(): string
    {
        return self::$addonTemplatesPath;
    }

    public static function setAddonTemplatesPath(string $value)
    {
        self::$addonTemplatesPath = $value;
    }

    public static function getAddonValue(string $value): string
    {
        return !empty(self::$addon[$value]) ? self::$addon[$value] : '';
    }
}
