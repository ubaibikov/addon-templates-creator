<?php

namespace Construction\CreateHlprs;

class CreateHlpr
{
    /**
     * @param array  $createLbls :  Is adddon label dir/files
     * @param string $nedded     : Is a addon [file_bland]
     * @param string $addonName  : Is a addon Name
     *
     * @return array $createCurrentLbls : Current dir path for creating addon_files / folders
     */
    public static function createCurrentAddonLabels(array $createLbls, string $nedded, string $addonName): array
    {
        $createLbls = self::createLabels($createLbls);
        $createCurrentLbls = [];

        foreach ($createLbls as $keyLabel => $addon) {
            $createCurrentLbls[$keyLabel] = self::labelCurrentPth($nedded, $addonName, $addon);
        }

        return $createCurrentLbls;
    }

    /**
     * @param array $addon : Is a addon dirs
     *
     * @return array $createLbls : Is a addon dirs with DIR_ROOT
     */
    private static function createLabels(array $addon): array
    {
        $createLbls = [];
        foreach ($addon as $key_addon => $addon_root) {
            $createLbls[$key_addon] = DIR_ROOT.$addon_root;
        }

        return $createLbls;
    }

    public static function labelCurrentPth(string $nedded, string $addonName, string $addon): string
    {
        return str_replace($nedded, $addonName, $addon);
    }

    public static function currentPathKey(string $nedded, string $pathKey): bool
    {
        if (preg_match("/$nedded/", $pathKey)) {
            return true;
        }

        return false;
    }
}
