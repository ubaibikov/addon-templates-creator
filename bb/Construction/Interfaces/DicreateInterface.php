<?php

namespace Construction\Interfaces;

interface DicreateInterface
{
    public function create(array $createLbls);

    public function createLbs(array $createLbls);

    public function conCreate(string $pth): bool;
}
