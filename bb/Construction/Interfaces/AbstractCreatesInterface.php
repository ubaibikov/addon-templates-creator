<?php

namespace Construction\Interfaces;

interface AbstractCreatesInterface
{
    public function getState(): bool;
}
