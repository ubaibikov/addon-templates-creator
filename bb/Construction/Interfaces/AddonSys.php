<?php

namespace Construction\Interfaces;

interface AddonSys
{
    public static function getAddonLabels(): array;

    public static function setAddonLabel(string $key, string $value);
}
