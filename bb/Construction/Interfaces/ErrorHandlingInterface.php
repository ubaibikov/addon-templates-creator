<?php

namespace Construction\Interfaces;

interface ErrorHandlingInterface
{
    public function errorReport(string $message, string $pth): string;
}
