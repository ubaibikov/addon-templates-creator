<?php

namespace Construction;

use  Construction\Abstracts\AbstractAddon;
use Construction\Interfaces\AddonSys;

class AddonFls extends AbstractAddon implements AddonSys
{
    protected static $addon = [
        'addon' => '/app/addons/[addon_name]/addon.xml',
        'en'    => '/var/langs/en/addons/[addon_name].po',
        'ru'    => '/var/langs/ru/addons/[addon_name].po',
        'func'  => '/app/addons/[addon_name]/func.php',
        'init'  => '/app/addons/[addon_name]/init.php',
        'copy'  => '/design/backend/media/images/addons/[addon_name]/icon.png',
    ];

    public static function getAddonLabels(): array
    {
        return self::$addon;
    }

    public static function setAddonLabel(string $key, string $value)
    {
        $addon = self::$addon;

        if (!empty($addon[$key])) {
            $addon[$key] = $value;
        } else {
            return;
        }
    }
}
