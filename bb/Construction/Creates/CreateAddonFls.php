<?php

namespace Construction\Creates;

use Construction\Abstracts\AbstractCreates;
use Construction\Interfaces\AbstractCreatesInterface;
use Construction\Interfaces\DicreateInterface;
use Construction\Interfaces\ErrorHandlingInterface;

class CreateAddonFls extends AbstractCreates implements ErrorHandlingInterface, DicreateInterface, AbstractCreatesInterface
{
    public function create(array $createLbls)
    {
        return $this->createLbs($createLbls);
    }

    public function createLbs(array $createLbs): array
    {
        foreach ($createLbs as $lbs_key => $lbs) {
            if (!file_exists($lbs)) {
                $file_handle = $this->conCreate($lbs);
                if ($file_handle) {
                    $this->state = true;
                }
            } else {
                $this->messages[] = $this->errorReport('Файл уже существует', $lbs);
                $this->state = false;
            }
        }

        return $this->state ? $createLbs : $this->messages;
    }

    public function conCreate(string $pth): bool
    {
        $handle = fopen($pth, 'w+');
        fclose($handle);

        return $handle ? true : false;
    }

    public function errorReport(string $message, string $pth): string
    {
        return "$message : [$pth]";
    }

    public function getState(): bool
    {
        return $this->state;
    }

    public function getMessgaes(): array
    {
        return $this->messages;
    }
}
