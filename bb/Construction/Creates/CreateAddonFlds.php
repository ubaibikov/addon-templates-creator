<?php

namespace Construction\Creates;

use Construction\Abstracts\AbstractCreates;
use Construction\Interfaces\AbstractCreatesInterface;
use Construction\Interfaces\DicreateInterface;
use Construction\Interfaces\ErrorHandlingInterface;

class CreateAddonFlds extends AbstractCreates implements DicreateInterface, ErrorHandlingInterface, AbstractCreatesInterface
{
    private $lbsKeys = [];

    public function create(array $createLbls)
    {
        return $this->createLbs($createLbls);
    }

    public function createLbs(array $createLbls)
    {
        foreach ($createLbls as $lbs_key => $lbs) {
            if (!is_dir($lbs)) {
                $folder_handle = $this->conCreate($lbs);
                if ($folder_handle) {
                    $this->lbsKeys[$lbs_key] = $lbs;
                    $this->state = true;
                }
            } else {
                $this->messages[] = $this->errorReport('Директория', "$lbs :существует");
                $this->state = false;
            }
        }
    }

    public function conCreate(string $pth): bool
    {
        $handle = mkdir($pth, 0755);

        return $handle;
    }

    public function errorReport(string $message, string $pth): string
    {
        return "$message --- [$pth]";
    }

    public function getState(): bool
    {
        return $this->state;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function getCreatedLabels(): array
    {
        return $this->lbsKeys;
    }
}
