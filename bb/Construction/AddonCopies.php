<?php

namespace Construction;

use Construction\Abstracts\AbstractAddon;
use Construction\Interfaces\AddonSys;

class AddonCopies extends AbstractAddon implements AddonSys
{
    protected static $addon = [
        'copy' => 'resources/icon/icon.png',
    ];

    public static function getAddonLabels(): array
    {
        return self::$addon;
    }

    public static function setAddonLabel(string $key, string $value)
    {
        self::$addon[$key] = $value;
    }

    public static function getAddonCopiesValue(string $key): string
    {
        return self::$addon[$key];
    }
}
