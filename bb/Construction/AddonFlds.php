<?php

namespace Construction;

use  Construction\Abstracts\AbstractAddon;
use Construction\Interfaces\AddonSys;

class AddonFlds extends AbstractAddon implements AddonSys
{
    protected static $addon = [
        'addon' => '/app/addons/[addon_name]',
        'copy'  => '/design/backend/media/images/addons/[addon_name]',
    ];

    public static function getAddonLabels(): array
    {
        return self::$addon;
    }

    public static function setAddonLabel(string $key, string $value)
    {
        $addon = self::$addon;

        if (!empty($addon[$key])) {
            $addon[$key] = $value;
        } else {
            return;
        }
    }
}
